function Test-GitRepo {
    return [System.Convert]::ToBoolean((git rev-parse --is-inside-work-tree))
}

function Get-Commits ($limit) {
    
    if (!(Test-GitRepo)) {
        Write-Error (Get-Item -Path ".\" -Verbose).FullName is not part of a git repository
        return;
    }

    if($limit -eq $null -or $limit -le 0){
        $limit = 10
    }

    return (git log --format="%ai`t%H`t%h`t%an`t%ae`t%s" -n $limit) | 
            ConvertFrom-Csv -Delimiter "`t" -Header ("Date","CommitId", "ShortCommit","Author","Email","Subject")

}

function Get-CommitFiles ($id) {
    
    if (!(Test-GitRepo)) {
        Write-Error (Get-Item -Path ".\" -Verbose).FullName is not part of a git repository
        return;
    }

    return (git diff-tree --no-commit-id --name-only -r $id)
}