How to use?
=============

Use the $profile variable in powershell to find where your powershell profile is saved. See the sample profile for a simple profile that loads autoload scripts.

Git Commands
=============

Get-Commits [-limit n]
Get-CommitFiles short_commit_id

git difftool $commit^! <file>