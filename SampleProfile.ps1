# directory where my scripts are stored

$psdir="E:\Projects\Powershell\Scripts\autoload"  

# load all 'autoload' scripts

Get-ChildItem "${psdir}\*.ps1" | %{.$_} 

Write-Host "Custom PowerShell Environment Loaded" 