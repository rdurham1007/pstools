﻿$shell = New-Object -com "Shell.Application"
$folder = $shell.Namespace("$env:APPDATA\Microsoft\Internet Explorer\Quick Launch\User Pinned\TaskBar")
$item = $folder.ParseName('Servicedesk-chat.lnk')
if(-Not $item) { return }
$verb = $item.Verbs() | where Name -like '*Unpin*'
if($verb) { $verb.DoIt() }